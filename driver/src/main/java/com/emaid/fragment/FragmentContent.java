package com.emaid.fragment;

import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.aretha.slidemenu.SlideMenu;
import com.emaid.HomepageActivity;
import com.emaid.Login;
import com.emaid.R;
import com.emaid.Synchronize;
import com.emaid.baseinterface.BaseInterface;
import com.emaid.database.maid.MySQLiteHelper;
import com.emaid.services.MaidattendanceServices;
import com.emaid.utils.CommonUtil;
import com.emaid.utils.URLUtil;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FragmentContent extends Fragment implements BaseInterface {
    View view;
    LinearLayout _lytMenu, _lytcalender, _lytMaidAttendance;

    SlideMenu slideMenu;

    View convertview;
    View Secondary, Primary;
    public ArrayList<HashMap<String, String>> _arrayDetails = new ArrayList<HashMap<String, String>>();
    TextView _txtDate;
    LinearLayout _layoutRefresh;
    ListView listattendance;
    MySQLiteHelper mySQLiteHelper;
    int position;
    Listadapter1 tester;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        view = inflater.inflate(R.layout.fragment_contentpagechild, null);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        initUI();
        clickFunction();
        menuselection();
        dataSelection();

    }

    @Override
    public void initUI() {
        mySQLiteHelper = new MySQLiteHelper(getActivity());
        _lytMaidAttendance = (LinearLayout) view.findViewById(R.id.fragmnetcontentchild_lyt_maidattendance);
        _lytMenu = (LinearLayout) view.findViewById(R.id.fragmnetcontentchild_lyt_menu);
        _lytcalender = (LinearLayout) view.findViewById(R.id.fragmnetcontentchild_lyt_calender);
        _txtDate = (TextView) view.findViewById(R.id.fragmnetcontentchild_txt_date);
        _layoutRefresh = (LinearLayout) view.findViewById(R.id.fragmnetcontentchild_lyt_refresh);

        TextView textViewHeading = (TextView) view.findViewById(R.id.fragmnetcontentchild_txt_heading);
        textViewHeading.setText("Driver Module\n" + URLUtil._strZoneName);
        slideMenu = ((HomepageActivity) getActivity()).slideMenu;
        Secondary = ((HomepageActivity) getActivity()).secondaryMenu;
        Primary = ((HomepageActivity) getActivity()).primaryMenu;

    }

    public void dataSelection() {
        LinearLayout layoutToday = (LinearLayout) Primary.findViewById(R.id.fragmentdataselectionchild_lyt_today);
        LinearLayout lytLogoutTomorrow = (LinearLayout) Primary.findViewById(R.id.fragmentdataselectionchild_lyt_tomorrow);

        layoutToday.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                _txtDate.setText("Today");
                FragmentManager fragmentManager = getChildFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment fragment = FragmentTodayschedule.instance(1);
                fragmentTransaction.replace(R.id.fragmnetcontentchild_flyt, fragment);
                fragmentTransaction.commitAllowingStateLoss();

            }
        });

        lytLogoutTomorrow.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                _txtDate.setText("Tomorrow");
                FragmentManager fragmentManager = getChildFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment fragment = FragmentTodayschedule.instance(2);
                fragmentTransaction.replace(R.id.fragmnetcontentchild_flyt, fragment);
                fragmentTransaction.commitAllowingStateLoss();

            }
        });
    }

    public void menuselection() {

        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment fragment = FragmentTodayschedule.instance(1);
        fragmentTransaction.replace(R.id.fragmnetcontentchild_flyt, fragment);
        fragmentTransaction.commitAllowingStateLoss();

        LinearLayout layout = (LinearLayout) Secondary.findViewById(R.id.fragmentmenu_lyt_attendance);
        LinearLayout lytLogout = (LinearLayout) Secondary.findViewById(R.id.fragmentmenu_lyt_logout);
        LinearLayout lytSynchronise = (LinearLayout) Secondary.findViewById(R.id.fragmentmenu_lyt_synchronise);
        LinearLayout lytPaymentHistory = (LinearLayout) Secondary.findViewById(R.id.fragmentmenu_lyt_paymnethistory);
//		LinearLayout lytAttendanceReport = (LinearLayout) Secondary.findViewById(R.id.fragmentmenu_lyt_attendancereport);
        layout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                FragmentManager fragmentManager = getChildFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment fragment = FragmentTodayschedule.instance(1);
                fragmentTransaction.replace(R.id.fragmnetcontentchild_flyt, fragment);
                fragmentTransaction.commitAllowingStateLoss();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {

                        slideMenu.close(true);
                    }
                }, 100);

            }
        });

        lytLogout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                URLUtil.edit.putBoolean("login", false);
                URLUtil.edit.commit();
                Intent intent = new Intent(getActivity(), Login.class);
                startActivity(intent);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

            }
        });
        lytSynchronise.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                Intent intent = new Intent(getActivity(), Synchronize.class);
                intent.putExtra("page", "0");
                startActivity(intent);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

            }
        });
        lytPaymentHistory.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                slideMenu.close(true);
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        FragmentManager fragmentManager = getChildFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        Fragment fragment = PaymentHistoryFragment.instance();
                        fragmentTransaction.replace(R.id.fragmnetcontentchild_flyt, fragment);
                        fragmentTransaction.commitAllowingStateLoss();

                    }
                }, 100);

            }
        });

//		lytAttendanceReport.setOnClickListener(new OnClickListener()
//		{
//
//			@Override
//			public void onClick(View arg0)
//			{
//				slideMenu.close(true);
//				new Handler().postDelayed(new Runnable()
//				{
//
//					@Override
//					public void run()
//					{
//						FragmentManager fragmentManager = getChildFragmentManager();
//						FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//						Fragment fragment = AttendanceReportFragment.instance();
//						fragmentTransaction.replace(R.id.fragmnetcontentchild_flyt, fragment);
//						fragmentTransaction.commitAllowingStateLoss();
//
//					}
//				}, 100);
//
//			}
//		});
    }

    @Override
    public void clickFunction() {

        _lytMenu.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                slideMenu.open(true, true);
            }
        });
        _lytcalender.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                slideMenu.open(false, true);
            }
        });

        _layoutRefresh.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                CommonUtil.synchrinoouserror(getActivity(), "Confirmation", "Do you want to Refresh ?");
            }
        });

        _lytMaidAttendance.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                getData(URLUtil.getAttendanceReport());

            }
        });

    }

    public void getData(String URL) {
        CommonUtil.progressDialogue(getActivity());
        CommonUtil.dialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(30000);
        client.get(URL, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(Throwable arg0, String arg1) {
                // TODO Auto-generated method stub
                super.onFailure(arg0, arg1);
                CommonUtil.dialog.dismiss();
                CommonUtil.CustomeDialogue(getActivity(), "Error", "Network Error");
            }

            @Override
            public void onSuccess(String arg0) {
                // TODO Auto-generated method stub
                CommonUtil.dialog.dismiss();
                synchrinoousattendance(getActivity(), "Attendance Details");
                super.onSuccess(arg0);

                Gson gson = new Gson();
                try {
                    EmaidCustomerAttendanceReportDetailsURL oEmaidCustomerAttendanceReportDetailsURL = gson.fromJson(arg0, EmaidCustomerAttendanceReportDetailsURL.class);
                    if (oEmaidCustomerAttendanceReportDetailsURL.getStatus().equals("success")) {

//						Listadapter1 oListadapter = new Listadapter1(oEmaidCustomerAttendanceReportDetailsURL.getAttandencemaid());
//						listattendance.setAdapter(oListadapter);
                        // double amount = 0;
                        // for (int i = 0; i <
                        // oEmaidCustomerAttendanceReportDetailsURL.getPayments().size();
                        // i++)
                        // {
                        // if
                        // (oEmaidCustomerAttendanceReportDetailsURL.getPayments().get(i).getPayment_status().equals("1"))
                        // {
                        // amount = amount +
                        // Double.parseDouble(oEmaidCustomerAttendanceReportDetailsURL.getPayments().get(i).getPaid_amount());
                        // }
                        //
                        // }
                        // textViewTotal.setText("Total : " +
                        // String.valueOf(amount));
                        tester = new Listadapter1(
                                oEmaidCustomerAttendanceReportDetailsURL.getAttandencemaid());
                        listattendance.setAdapter(tester);
                    } else {

                        CommonUtil.CustomeDialogue(getActivity(), "Error", oEmaidCustomerAttendanceReportDetailsURL.getStatus());
                    }
                } catch (Exception e) {
                    CommonUtil.CustomeDialogue(getActivity(), "Error", "Network Error");
                }

            }

        });
    }

    public void synchrinoousattendance(final FragmentActivity activity, String string) {
        ActivityManager am = (ActivityManager) activity.getSystemService(activity.ACTIVITY_SERVICE);

        final Dialog dialog1 = new Dialog(activity);

        dialog1.setContentView(R.layout.customdialogue_maidattendance);

        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog1.setCancelable(true);
        TextView textViewHeading = (TextView) dialog1.findViewById(R.id.customeerror_heading);
        // TextView textViewContent = (TextView)
        // dialog1.findViewById(R.id.customeerror_message);
        // Button buttonOk = (Button)
        // dialog1.findViewById(R.id.customeerror_btn);
        textViewHeading.setText(string.trim());
        // textViewContent.setText(_strMessage.trim());
        listattendance = (ListView) dialog1.findViewById(R.id.listView1);

        //
        // buttonOk.setOnClickListener(new OnClickListener()
        // {
        //
        // @Override
        // public void onClick(View arg0)
        // {
        // // TODO Auto-generated method stub
        //
        // SynchronizeCustomer.diologueSynchronize(activity, "1");
        //
        // dialog1.dismiss();
        // }
        // });

        dialog1.show();

    }

    public class Listadapter1 extends BaseAdapter {
        LayoutInflater inflater;
        List<EmaidCustomerAttendanceReportDetailsURLContainer> list = new ArrayList<EmaidCustomerAttendanceReportDetailsURLContainer>();

        //		List<EmaidCustomerAttendanceReportDetailsURLContainer> list1new;
        public Listadapter1(List<EmaidCustomerAttendanceReportDetailsURLContainer> list1) {
//		 list1new = list1;
            inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            for (int i = 0; i < list1.size(); i++) {
                // if (list1.get(i).getPayment_status().equals("1"))
                // {
                list.add(list1.get(i));
                // }
            }

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return list.size();
        }

        @Override
        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(final int arg0, View arg1, ViewGroup arg2) {

//			 position=arg0;
            View view = inflater.inflate(R.layout.adapter_grid_attendancereport, null);
            LinearLayout layout = (LinearLayout) view.findViewById(R.id.adapterpayment_layout_customername);
            Button buttonCustomername = (Button) view.findViewById(R.id.adapterpayment_btn_customername);

            final Button buttonCustomerPhoneNo = (Button) view.findViewById(R.id.adapterpayment_btn_customerphoneno);

            buttonCustomername.setText(list.get(arg0).getMaid_name());


//			try
//			{
//				if (list.get(arg0).getScheduled_zone().equalsIgnoreCase(URLUtil._strZoneName)) 
//				{
//				view.setBackgroundColor(Color.parseColor("#CCCCCC"));
//				}
//			}
//			catch (Exception e)
//			{
//				// TODO: handle exception
//			}


            // buttonCustomerAddress.setText(list.get(arg0).getCustomer_address());
            try {


                if (list.get(arg0).getAttandence_status().equalsIgnoreCase("IN")) {
                    if (list.get(arg0).getZone_name().equalsIgnoreCase(URLUtil._strZoneName)) {

                        position = arg0;

                        System.out.println("in IN loop" + position);
                        buttonCustomerPhoneNo.setBackgroundDrawable(getResources().getDrawable(R.drawable.maidoutneww));
                        buttonCustomerPhoneNo.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                // TODO Auto-generated method stub
                                position = arg0;
                                mySQLiteHelper.setMaidAttendance(list.get(position).getMaid_id(), "2");
                                buttonCustomerPhoneNo.setBackgroundDrawable(getResources().getDrawable(R.drawable.maidin));

                                System.out.println("buttonCustomerPhoneNoin IN loop" + position);
//							list.get(position).setAttandence_status("OUT");
//							list.set(position, object);
//							test.notifyDataSetChanged();
                                Intent i = new Intent(getActivity(), MaidattendanceServices.class);
                                i.putExtra("attendance", "2");

                                i.putExtra("maidid", list.get(position).getMaid_id());
                                EmaidCustomerAttendanceReportDetailsURLContainer object = new EmaidCustomerAttendanceReportDetailsURLContainer();
                                object.setAttandence_status("OUT");
                                object.setMaid_id(list.get(position).getMaid_id());
                                object.setMaid_name(list.get(position).getMaid_name());
                                object.setTablet_id(URLUtil.strPhoneIMEI);
                                object.setZone_name(URLUtil._strZoneName);
                                object.setScheduled_zone(list.get(position).getScheduled_zone());
                                list.set(position, object);
                                Log.e("", "" + list.get(position).getAttandence_status());
                                Log.e("", "" + list.get(position).getMaid_id());
                                Log.e("", "" + list.get(position).getMaid_name());
                                Log.e("", "" + list.get(position).getTablet_id());
                                Log.e("", "" + list.get(position).getZone_name());
//							list1new.set(position, object);
                                tester.notifyDataSetChanged();
                                getActivity().startService(i);

                            }
                        });

                    } else {

                        position = arg0;
                        buttonCustomerPhoneNo.setText(list.get(position).getZone_name());
                        buttonCustomerPhoneNo.setClickable(false);
                        tester.notifyDataSetChanged();

                    }
//				 position=arg0;
//				 
//				 System.out.println("in IN loop"+position);
//				buttonCustomerPhoneNo.setBackgroundDrawable(getResources().getDrawable(R.drawable.maidout));
//				buttonCustomerPhoneNo.setOnClickListener(new OnClickListener()
//				{
//
//					@Override
//					public void onClick(View v)
//					{
//						// TODO Auto-generated method stub
//						position=arg0;
//						mySQLiteHelper.setMaidAttendance(list.get(position).getMaid_id(), "2");
//						buttonCustomerPhoneNo.setBackgroundDrawable(getResources().getDrawable(R.drawable.maidin));
//						
//						System.out.println("buttonCustomerPhoneNoin IN loop"+position);
////						list.get(position).setAttandence_status("OUT");
////						list.set(position, object);
////						test.notifyDataSetChanged();
//						Intent i = new Intent(getActivity(), MaidattendanceServices.class);
//						i.putExtra("attendance", "2");
//
//						i.putExtra("maidid", list.get(position).getMaid_id());
//						EmaidCustomerAttendanceReportDetailsURLContainer object = new EmaidCustomerAttendanceReportDetailsURLContainer();
//						object.setAttandence_status("OUT");
//						object.setMaid_id(list.get(position).getMaid_id());
//						object.setMaid_name(list.get(position).getMaid_name());
//						object.setTablet_id(list.get(position).getTablet_id());
//						object.setZone_name(list.get(position).getZone_name());
//						list.set(position, object);
//						Log.e("", ""+list.get(position).getAttandence_status());
//						Log.e("", ""+list.get(position).getMaid_id());
//						Log.e("", ""+list.get(position).getMaid_name());
//						Log.e("", ""+list.get(position).getTablet_id());
//						Log.e("", ""+list.get(position).getZone_name());
////						list1new.set(position, object);
//						tester.notifyDataSetChanged();
//						getActivity().startService(i);
//
//					}
//				});
                } else if (list.get(arg0).getAttandence_status().equalsIgnoreCase("OUT")) {
                    position = arg0;
                    System.out.println("in OUT loop" + position);
                    buttonCustomerPhoneNo.setBackgroundDrawable(getResources().getDrawable(R.drawable.maidin));
                    buttonCustomerPhoneNo.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            position = arg0;
                            mySQLiteHelper.setMaidAttendance(list.get(position).getMaid_id(), "1");
                            buttonCustomerPhoneNo.setBackgroundDrawable(getResources().getDrawable(R.drawable.maidoutneww));
                            System.out.println("buttonCustomerPhoneNoin OUT loop" + position);
//						list.get(position).setAttandence_status("IN");
//						test.notifyDataSetChanged();
                            Intent i = new Intent(getActivity(), MaidattendanceServices.class);
                            i.putExtra("attendance", "1");

                            i.putExtra("maidid", list.get(position).getMaid_id());
                            EmaidCustomerAttendanceReportDetailsURLContainer object = new EmaidCustomerAttendanceReportDetailsURLContainer();
                            object.setAttandence_status("IN");
                            object.setMaid_id(list.get(position).getMaid_id());
                            object.setMaid_name(list.get(position).getMaid_name());
                            object.setTablet_id(URLUtil.strPhoneIMEI);
                            object.setZone_name(URLUtil._strZoneName);
                            object.setScheduled_zone(list.get(position).getScheduled_zone());
                            list.set(position, object);
                            Log.e("", "" + list.get(position).getAttandence_status());
                            Log.e("", "" + list.get(position).getMaid_id());
                            Log.e("", "" + list.get(position).getMaid_name());
                            Log.e("", "" + list.get(position).getTablet_id());
                            Log.e("", "" + list.get(position).getZone_name());
//						list1new.set(position, object);

                            tester.notifyDataSetChanged();
                            getActivity().startService(i);

                        }
                    });

                } else {
//				position=arg0;
//				buttonCustomerPhoneNo.setText(list.get(position).getZone_name());
//				buttonCustomerPhoneNo.setClickable(false);
//				tester.notifyDataSetChanged();
                }
            } catch (Exception e) {
                // TODO: handle exception

                position = arg0;
                System.out.println("in OUT loop" + position);
                buttonCustomerPhoneNo.setBackgroundDrawable(getResources().getDrawable(R.drawable.maidin));
                buttonCustomerPhoneNo.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        position = arg0;
                        mySQLiteHelper.setMaidAttendance(list.get(position).getMaid_id(), "1");
                        buttonCustomerPhoneNo.setBackgroundDrawable(getResources().getDrawable(R.drawable.maidoutneww));
                        System.out.println("buttonCustomerPhoneNoin OUT loop" + position);
//						list.get(position).setAttandence_status("IN");
//						test.notifyDataSetChanged();
                        Intent i = new Intent(getActivity(), MaidattendanceServices.class);
                        i.putExtra("attendance", "1");

                        i.putExtra("maidid", list.get(position).getMaid_id());
                        EmaidCustomerAttendanceReportDetailsURLContainer object = new EmaidCustomerAttendanceReportDetailsURLContainer();
                        object.setAttandence_status("IN");
                        object.setMaid_id(list.get(position).getMaid_id());
                        object.setMaid_name(list.get(position).getMaid_name());
                        object.setTablet_id(URLUtil.strPhoneIMEI);
                        object.setZone_name(URLUtil._strZoneName);
                        object.setScheduled_zone(list.get(position).getScheduled_zone());
                        list.set(position, object);
                        Log.e("", "" + list.get(position).getAttandence_status());
                        Log.e("", "" + list.get(position).getMaid_id());
                        Log.e("", "" + list.get(position).getMaid_name());
                        Log.e("", "" + list.get(position).getTablet_id());
                        Log.e("", "" + list.get(position).getZone_name());
//						list1new.set(position, object);

                        tester.notifyDataSetChanged();
                        getActivity().startService(i);

                    }
                });
            }

            // buttonCustomernameAmount.setText(list.get(arg0).getPaid_amount());
            // }

            return view;
        }

    }

}
