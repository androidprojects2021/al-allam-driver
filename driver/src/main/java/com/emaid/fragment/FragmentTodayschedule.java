package com.emaid.fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.emaid.R;
import com.emaid.DeatilsActivity;
import com.emaid.Synchronize;
import com.emaid.model.EmaidCustomerDetailsURLContainer;
import com.emaid.baseinterface.BaseInterface;
import com.emaid.baseinterface.GlobalManager;
import com.emaid.database.maid.ImageLoderutil;
import com.emaid.database.maid.MySQLiteHelper;
import com.emaid.notification.Myservices;
import com.emaid.utils.CommonUtil;
import com.emaid.utils.URLUtil;

public class FragmentTodayschedule extends Fragment implements BaseInterface {
	View view;

	LinearLayout _listContent;

	ViewHolder holder;
	LayoutInflater inflater;
	LayoutInflater inflater1;
	View convertview;
	List<EmaidCustomerDetailsURLContainer> containers = new ArrayList<EmaidCustomerDetailsURLContainer>();
	MySQLiteHelper helper;

	static int intdate = 1;

	public static FragmentTodayschedule instance(int date)
	{
		FragmentTodayschedule fragmentTodayschedule = new FragmentTodayschedule();

		intdate = date;
		return fragmentTodayschedule;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		view = inflater.inflate(R.layout.fragment_todayschedule, null);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		GlobalManager.getdata().setCurrentActivity("HomepageActivity");
		initUI();
		clickFunction();

	}

	@Override
	public void onResume()
	{
		// TODO Auto-generated method stub
		super.onResume();
		GlobalManager.getdata().setCurrentActivity("HomepageActivity");
		if (!URLUtil.strSynchronizeDate.equalsIgnoreCase(URLUtil.getCurrentDate(getActivity())))
		{
			// Intent oIntent = new Intent(getActivity(), Synchronize.class);
			// startActivity(oIntent);
			// getActivity().finish();
			// getActivity().overridePendingTransition(R.anim.slide_in,
			// R.anim.slide_out);
			Intent myIntent = new Intent(getActivity().getApplicationContext(), Myservices.class);
			myIntent.putExtra("message", "Customer Update Requried");
			myIntent.putExtra("heading", "Update Requried");
			getActivity().getApplicationContext().startService(myIntent);
		}
		else
		{

			upDateUI();
		}

	}

	@Override
	public void initUI()
	{

		_listContent = (LinearLayout) view.findViewById(R.id.fragmnetcontentchild_list_content);

		ImageLoderutil.retriveMaid(getActivity());
		helper = new MySQLiteHelper(getActivity());

	}

	@Override
	public void clickFunction()
	{

	}

	public void upDateUI()
	{
		CommonUtil.progressDialogue(getActivity());
		CommonUtil.dialog.show();
		getActivity().runOnUiThread(new Runnable()
		{

			@Override
			public void run()
			{
				// TODO Auto-generated method stub

				ContentAdapter();
			}
		});
	}

	public void ContentAdapter()
	{

		_listContent.removeAllViews();
		inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater1 = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		holder = new ViewHolder();

		for (int j = 0; j < 24; j++)
		{
			containers.clear();
			String hr = String.valueOf(j);
			if (hr.length() == 1)
			{
				hr = "0" + hr;
			}

			if (intdate == 2)
			{
				containers = helper.returncustomerTomorrow(hr + ":00",hr + ":30");
			}
			else
			{
				containers = helper.returncustomer(hr + ":00",hr + ":30");
			}

			if (containers.size() > 0)
			{

				dataBinding(containers, j);
			}
 
		}

		CommonUtil.dialog.dismiss();

	}

	public void dataBinding(List<com.emaid.model.EmaidCustomerDetailsURLContainer> containers2, int j)
	{
		// int count = 0;
		convertview = inflater.inflate(R.layout.adapter_content_mainlist, null);
		holder._lytContent = (LinearLayout) convertview.findViewById(R.id.adaptercontentmain_lyt);

		holder._txtStartTime = (TextView) convertview.findViewById(R.id.adaptercontentmain_starttime);
		holder._txtTotalMaids = (TextView) convertview.findViewById(R.id.adaptercontentmain_totalmaids);
		holder._txtTotalMaids.setText(containers2.size() + " Maids");
		holder._txtStartTime.setText(hr12Format(containers2.get(0).getShift_start()));

		for (int i = 0; i < containers2.size(); i++)
		{

			View childview = inflater1.inflate(R.layout.adapter_singlecell_list, null);
			LinearLayout layout = (LinearLayout) childview.findViewById(R.id.adaptersinglecell_main);

			holder._txtMaidName = (TextView) childview.findViewById(R.id.adaptersinglecell_maidname);
			holder._txtCustomername = (TextView) childview.findViewById(R.id.adaptersinglecell_customername);
			holder._txtCustomerid = (TextView) childview.findViewById(R.id.adaptersinglecell_id);
			holder._txtShifttime = (TextView) childview.findViewById(R.id.adaptersinglecell_shifttime);
			holder._txtCustomerAddress = (TextView) childview.findViewById(R.id.adaptersinglecell_address);
			holder._ivPhoto = (ImageView) childview.findViewById(R.id.adaptersinglecell_maidimage);
			holder.progressBar = (ProgressBar) childview.findViewById(R.id.adaptersinglecell_progressbar);
			holder.cleanIcon = (ImageView) childview.findViewById(R.id.cleanIcon);
			
			if (containers2.get(i).getService_status().equalsIgnoreCase("0"))
			{
				layout.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.pending_bg));
			}
			else if (containers2.get(i).getService_status().equalsIgnoreCase("1"))
			{
				layout.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.onduty_bg));
			}
			else
			{
				layout.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.completed_bg));
			}
			layout.setTag(containers2.get(i).getBooking_id());

			holder._txtMaidName.setText(containers2.get(i).getMaid_name());
			holder._txtCustomername.setText(containers2.get(i).getCustomer_name());
			holder._txtCustomerid.setText(containers2.get(i).getCustomer_mobile());
			holder._txtShifttime.setText(hr12Format(containers2.get(i).getShift_start()) + " - " + hr12Format(containers2.get(i).getShift_end()));
			holder._txtCustomerAddress.setText(containers2.get(i).getCustomer_address());
			if(containers2.get(i).getCleaningMatte().equalsIgnoreCase("yes")){
				holder.cleanIcon.setVisibility(View.VISIBLE);
			}else{
				holder.cleanIcon.setVisibility(View.GONE);
			}
			ImageLoderutil.imageBinding(getActivity(), containers2.get(i).getMaid_id(), holder._ivPhoto, holder.progressBar);

			layout.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View arg1)
				{
					// TODO Auto-generated method stub

					Intent intent = new Intent(getActivity(), DeatilsActivity.class);
					intent.putExtra("bookingid", arg1.getTag().toString());
					intent.putExtra("date", intdate);
					startActivity(intent);
					getActivity().finish();

				}
			});

			holder._lytContent.addView(childview);

		}
		_listContent.addView(convertview);
	}

	static class ViewHolder {

		LinearLayout _lytContent;
		TextView _txtStartTime;
		TextView _txtCode;
		TextView _txtTotalMaids;
		TextView _txtRemainingMaids;
		TextView _txtMaidName;
		TextView _txtCustomername;
		TextView _txtCustomerid;
		TextView _txtCustomerAddress;
		TextView _txtShifttime;
		ImageView _ivPhoto;
		ProgressBar progressBar;
		ImageView cleanIcon;

	}

	public String hr12Format(String time1)
	{

		String Time = "";
		try
		{
			String _24HourTime = time1;
			SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
			SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
			java.util.Date _24HourDt = _24HourSDF.parse(_24HourTime);

			Time = _12HourSDF.format(_24HourDt);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Time = "00:00 AM";
		}

		return Time;
	}

}