package com.emaid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.WindowManager;

import com.emaid.baseinterface.GlobalManager;
import com.emaid.notification.Myservices;
import com.emaid.notification.NotificationDioalogue;

public class BaseActivity extends FragmentActivity {

    public static boolean appInFront;
    MyReceiver myReceiver = new MyReceiver();


    @Override
    protected void onCreate(Bundle arg0) {
        // TODO Auto-generated method stub
        super.onCreate(arg0);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        ServiceStart();
        appInFront = true;


    }

    @Override
    protected void onResume() {
        super.onStart();
        appInFront = true;

    }

    @Override
    protected void onPause() {
        super.onPause();
        appInFront = false;
        GlobalManager.getdata().setCurrentActivity("");
    }

    public void ServiceStart() {

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Myservices.MY_ACTION);
        registerReceiver(myReceiver, intentFilter);

    }


    private class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context arg0, Intent arg1) {
            // TODO Auto-generated method stub
            NotificationDioalogue.Notification(arg0, arg1.getExtras().getString("heading"), arg1.getExtras().getString("message"));
        }

    }


}
