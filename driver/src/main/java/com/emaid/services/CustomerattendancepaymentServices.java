package com.emaid.services;

import org.json.JSONObject;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.emaid.notification.Myservices;
import com.emaid.utils.URLUtil;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class CustomerattendancepaymentServices extends Service {
	String strattendance, strBookingid, strpayment, strAmount,strType,strOutstandingAmount,strReceipt;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		// TODO Auto-generated method stub

		strattendance = intent.getExtras().getString("attendance");
		strBookingid = intent.getExtras().getString("bookingid");
		strpayment = intent.getExtras().getString("payment");
		strAmount = intent.getExtras().getString("amount");
		strOutstandingAmount = intent.getExtras().getString("outstanding_amount");
		strReceipt = intent.getExtras().getString("receipt_no");
		strType = intent.getExtras().getString("type");

		callWebservices(URLUtil.getCustomerAttendance(strattendance, strBookingid,strpayment,strAmount,strOutstandingAmount,strReceipt,strType));
		return super.onStartCommand(intent, flags, startId);

	}

	public void callWebservices(String URL)
	
	{

		AsyncHttpClient client = new AsyncHttpClient();
		client.setTimeout(30000);
		client.get(URL, new AsyncHttpResponseHandler()
		{

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				// TODO Auto-generated method stub
				super.onFailure(arg0, arg1);

				fail();

			}

			@Override
			public void onSuccess(int arg0, String arg1)
			{
				// TODO Auto-generated method stub
				super.onSuccess(arg0, arg1);

				try
				
				{

					JSONObject jsonObjectq = new JSONObject(arg1);

					if (jsonObjectq.getString("status").equalsIgnoreCase("success"))
					{

					}
					else
					{

						fail();

					}

				}
				catch (Exception e)
				{
					fail();
				}

			}

		});

	}

	public void fail()
	{
		Intent myIntent = new Intent(getApplicationContext(), Myservices.class);
		myIntent.putExtra("message", "Customer attendance fail");
		myIntent.putExtra("heading", "Update Requried");
		getApplicationContext().startService(myIntent);
	}

	@Override
	public IBinder onBind(Intent arg0)
	{
		// TODO Auto-generated method stub
		return null;
	}

}
