package com.emaid.database.customer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;

import com.emaid.HomepageActivity;
import com.emaid.R;
import com.emaid.Synchronize;
import com.emaid.baseinterface.GlobalManager;
import com.emaid.database.maid.MySQLiteHelper;
import com.emaid.model.EmaidCustomerDetailsURL;
import com.emaid.model.EmaidCustomerDetailsURLContainer;
import com.emaid.utils.CommonUtil;
import com.emaid.utils.URLUtil;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class SynchronizeCustomer {
    private static Context _activity;
    private static ProgressDialog _objProgressDialog;
    private static MySQLiteHelper oMySQLiteHelper;
    private static int count = 0;
    private static String _id = "0";

    public static void diologueSynchronize(Context _context, String id) {

        _activity = _context;
        _id = id;
        _objProgressDialog = new ProgressDialog(_context);
        _objProgressDialog.setMessage("Synchronise customer....");
        _objProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        _objProgressDialog.setIndeterminate(false);
        _objProgressDialog.setProgress(0);
        _objProgressDialog.setCancelable(false);
        _objProgressDialog.show();

        oMySQLiteHelper = new MySQLiteHelper(_activity);
        oMySQLiteHelper.getWritableDatabase();

        count = 0;
        Calendar c = Calendar.getInstance();

        if (id.equalsIgnoreCase("2")) {
            getDatatomorrow(URLUtil.getCustomerdetails(id));
        } else {
            getData(URLUtil.getCustomerdetails(id));
        }

    }

    public static boolean retriveCustomer(Context a) {
        List<EmaidCustomerDetailsURLContainer> data = new ArrayList<EmaidCustomerDetailsURLContainer>();
        oMySQLiteHelper = new MySQLiteHelper(a);
        oMySQLiteHelper.getWritableDatabase();
        data.clear();

        data = oMySQLiteHelper.returncustomer();


        if (data.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static void getData(String URL) {

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(30000);
        client.get(URL, new AsyncHttpResponseHandler() {

            @SuppressWarnings("deprecation")
            @Override
            public void onFailure(Throwable arg0, String arg1) {
                // TODO Auto-generated method stub
                super.onFailure(arg0, arg1);
                URLUtil.edit.putBoolean("todaysynchronize", false);
                URLUtil.edit.commit();

                if (GlobalManager.getdata().getCurrentActivity().equals("Synchronize")) {
                    ((Synchronize) _activity).buttonSynchronizecustomer.setBackgroundDrawable(_activity.getResources().getDrawable(R.drawable.today_cross));
                }
                CommonUtil.CustomeDialogue(_activity, "Error", "Not Synchronize");
                _objProgressDialog.dismiss();

            }

            @SuppressWarnings("deprecation")
            @Override
            public void onSuccess(int arg0, String arg1) {
                // TODO Auto-generated method stub
                super.onSuccess(arg0, arg1);

                try {
                    Gson gson = new Gson();
                    EmaidCustomerDetailsURL oCustomerDetailsURL = gson.fromJson(arg1, EmaidCustomerDetailsURL.class);
                    oMySQLiteHelper.deleteTableCustomer();
                    _objProgressDialog.setMax(oCustomerDetailsURL.getSchedule().size());
                    if (oCustomerDetailsURL.getStatus().equalsIgnoreCase("success")) {
                        for (EmaidCustomerDetailsURLContainer tr : oCustomerDetailsURL.getSchedule()) {
                            count = count + 1;
                            _objProgressDialog.setProgress(count);
                            oMySQLiteHelper.addcustomerData(tr.getCustomer_id(), tr.getCustomer_code(), tr.getCustomer_name(), tr.getCustomer_address(), tr.getCustomer_mobile(),
                                    tr.getCustomer_type(), tr.getCustomer_status(), tr.getMaid_id(), tr.getMaid_name(), tr.getMaid_country(), tr.getMaid_status(), tr.getShift_start(),
                                    tr.getShift_end(), tr.getKey_status(), tr.getArea(), tr.getBooking_note(), tr.getService_status(), tr.getBooking_id(), tr.getService_fee(), tr.getCleaningMatte(), tr.getCustomer_c_latitude(), tr.getCustomer_c_longitude(), tr.getMaid_mobile(), tr.getCustomercare_mobile()); //abhijith added cleaning material

                            if (count == oCustomerDetailsURL.getSchedule().size()) {
                                _objProgressDialog.dismiss();

                            }
                        }

                        if (GlobalManager.getdata().getCurrentActivity().equals("Synchronize")) {
                            ((Synchronize) _activity).buttonSynchronizecustomer.setBackgroundDrawable(_activity.getResources().getDrawable(R.drawable.today_tick));
                        }
                        URLUtil.edit.putBoolean("todaysynchronize", true);

                        URLUtil.edit.putString("date", URLUtil.getCurrentDate(_activity));

                        URLUtil.edit.commit();
                        _objProgressDialog.dismiss();
                        if (GlobalManager.getdata().getCurrentActivity().equals("HomepageActivity")) {
                            Intent intent = new Intent(_activity, HomepageActivity.class);
                            _activity.startActivity(intent);
                            ((Activity) _activity).finish();
                        }
                    } else {
                        URLUtil.edit.putBoolean("todaysynchronize", false);
                        URLUtil.edit.commit();
                        if (GlobalManager.getdata().getCurrentActivity().equals("Synchronize")) {
                            ((Synchronize) _activity).buttonSynchronizecustomer.setBackgroundDrawable(_activity.getResources().getDrawable(R.drawable.today_cross));
                        }
                        CommonUtil.CustomeDialogue(_activity, "Error", "Not Synchronize");
                    }
                    _objProgressDialog.dismiss();

                } catch (Exception e) {
                    URLUtil.edit.putBoolean("todaysynchronize", false);
                    URLUtil.edit.commit();
                    if (GlobalManager.getdata().getCurrentActivity().equals("Synchronize")) {
                        ((Synchronize) _activity).buttonSynchronizecustomer.setBackgroundDrawable(_activity.getResources().getDrawable(R.drawable.today_cross));
                    }
                    CommonUtil.CustomeDialogue(_activity, "Error", "Not Synchronize");
                    _objProgressDialog.dismiss();

                }
            }

        });
    }

    public static void getDatatomorrow(String URL) {

        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(30000);
        client.get(URL, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(Throwable arg0, String arg1) {
                // TODO Auto-generated method stub
                super.onFailure(arg0, arg1);
                URLUtil.edit.putBoolean("tomorrowsynchronize", false);
                URLUtil.edit.commit();
                ((Synchronize) _activity).buttonSynchronizecustomerTomorrow.setBackgroundDrawable(_activity.getResources().getDrawable(R.drawable.tomorrow_cross));
                CommonUtil.CustomeDialogue(_activity, "Synchronize Fail", "Customer Details Not Synchronize");
                _objProgressDialog.dismiss();

            }

            @Override
            public void onSuccess(int arg0, String arg1) {
                // TODO Auto-generated method stub
                super.onSuccess(arg0, arg1);

                try {
                    Gson gson = new Gson();
                    EmaidCustomerDetailsURL oCustomerDetailsURL = gson.fromJson(arg1, EmaidCustomerDetailsURL.class);
                    oMySQLiteHelper.deleteTableCustomerTomorrow();
                    _objProgressDialog.setMax(oCustomerDetailsURL.getSchedule().size());
                    if (oCustomerDetailsURL.getStatus().equalsIgnoreCase("success")) {
                        for (EmaidCustomerDetailsURLContainer tr : oCustomerDetailsURL.getSchedule()) {
                            count = count + 1;
                            _objProgressDialog.setProgress(count);
                            oMySQLiteHelper.addcustomerDataTomorrow(tr.getCustomer_id(), tr.getCustomer_code(), tr.getCustomer_name(), tr.getCustomer_address(), tr.getCustomer_mobile(),
                                    tr.getCustomer_type(), tr.getCustomer_status(), tr.getMaid_id(), tr.getMaid_name(), tr.getMaid_country(), tr.getMaid_status(), tr.getShift_start(),
                                    tr.getShift_end(), tr.getKey_status(), tr.getArea(), tr.getBooking_note(), tr.getService_status(), tr.getBooking_id(), tr.getCleaningMatte(), tr.getCustomer_c_latitude(), tr.getCustomer_c_longitude(), tr.getMaid_mobile(), tr.getCustomercare_mobile()); //abhijith added for cleaning material

                            if (count == oCustomerDetailsURL.getSchedule().size()) {
                                _objProgressDialog.dismiss();
                            }
                        }

                        ((Synchronize) _activity).buttonSynchronizecustomerTomorrow.setBackgroundDrawable(_activity.getResources().getDrawable(R.drawable.tomorrow_tick));

                        URLUtil.edit.putString("date2", URLUtil.getCurrentDate(_activity));
                        URLUtil.edit.putBoolean("tomorrowsynchronize", true);

                        URLUtil.edit.commit();

                    } else {
                        URLUtil.edit.putBoolean("tomorrowsynchronize", false);
                        URLUtil.edit.commit();
                        ((Synchronize) _activity).buttonSynchronizecustomerTomorrow.setBackgroundDrawable(_activity.getResources().getDrawable(R.drawable.tomorrow_cross));
                        CommonUtil.CustomeDialogue(_activity, "Synchronize Fail", "Customer Details Not Synchronize");
                    }
                    _objProgressDialog.dismiss();

                } catch (Exception e) {
                    URLUtil.edit.putBoolean("tomorrowsynchronize", false);
                    URLUtil.edit.commit();
                    ((Synchronize) _activity).buttonSynchronizecustomerTomorrow.setBackgroundDrawable(_activity.getResources().getDrawable(R.drawable.tomorrow_cross));
                    CommonUtil.CustomeDialogue(_activity, "Synchronize Fail", "Customer Details Not Synchronize");
                    _objProgressDialog.dismiss();
                }
            }

        });
    }
}
