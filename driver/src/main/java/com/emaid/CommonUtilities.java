package com.emaid;

import android.content.Context;
import android.content.Intent;

import com.emaid.utils.URLUtil;

public final class CommonUtilities {

	// give your server registration URL here

	
//	public static final String SERVER_URL = "http://emaid.info/aa/api/registertablet?imei=" + URLUtil.strPhoneIMEI + "&regid="; // added on 6/1/17 live
	public static final String SERVER_URL = "http://emaid.info/aademo/api/registertablet?imei=" + URLUtil.strPhoneIMEI + "&regid="; // added on 6/1/17 demo
	
	public static final String SERVER_SECURITY = "";
	
	// Google project id
	public static final String SENDER_ID = "994437144603";
	
	/**
	 * Tag used on log messages.
	 */
	public static final String TAG = "EMaid";

	public static final String DISPLAY_MESSAGE_ACTION = "com.azinova.EMaid.DISPLAY_MESSAGE";

	public static final String EXTRA_MESSAGE = "message";

	/**
	 * Notifies UI to display a message.
	 * <p>
	 * This method is defined in the common helper because it's used both by the
	 * UI and the background service.
	 * 
	 * @param context
	 *            application's context.
	 * @param message
	 *            message to be displayed.
	 */
	public static void displayMessage(Context context, String message)
	{
		Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
		intent.putExtra(EXTRA_MESSAGE, message);
		context.sendBroadcast(intent);
	}
}
