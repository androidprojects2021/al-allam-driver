package com.emaid.model;

import java.util.List;

import com.emaid.model.EmaidCustomerDetailsURLContainer;

public class EmaidCustomerDetailsURL {
	
private String status = "";
	
	List<EmaidCustomerDetailsURLContainer> schedule ;

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public List<EmaidCustomerDetailsURLContainer> getSchedule()
	{
		return schedule;
	}

	public void setSchedule(List<EmaidCustomerDetailsURLContainer> schedule)
	{
		this.schedule = schedule;
	}

}
