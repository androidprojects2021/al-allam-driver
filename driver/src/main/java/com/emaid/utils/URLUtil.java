package com.emaid.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.provider.Settings.Secure;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class URLUtil {

    public static SharedPreferences oSharedPreferences;
    public static Editor edit;
    public static Boolean LOGIN;

    public static String strPhoneIMEI;
    public static String strSynchronizeDate, strSynchronizeDateTomorrow, _strZoneName;
    public static Boolean isTodaySynchronize, istomorrowSynchronize, ismadeSynchronize;


      // public static final String BASEURL = "http://emaid.info/aa/api/"; //added on 6/1/17 live url
   // private static final String BASEURL = "http://emaid.info/aademo/api/"; //added on 27/03/2017 demo url


   // private static final String BASEURL = "http://booking.emaid.info:8083/alallam/api/"; //added on 30/03/2020 live
    private static final String BASEURL = "http://emaid.info/aa_sab/api/"; //added on 05/01/2022 live  by Reema






    @SuppressWarnings("static-access")
    public static void activateSharepreference(Context _context) {
        oSharedPreferences = _context.getSharedPreferences("userdetails", _context.MODE_PRIVATE);
        edit = oSharedPreferences.edit();

        strPhoneIMEI = Secure.getString(_context.getContentResolver(), Secure.ANDROID_ID);
        // strPhoneIMEI = "15f20785682ab38d";
        LOGIN = oSharedPreferences.getBoolean("login", false);
        strSynchronizeDate = oSharedPreferences.getString("date", "");

        strSynchronizeDateTomorrow = oSharedPreferences.getString("date2", "");
        _strZoneName = oSharedPreferences.getString("zonename", "");
        isTodaySynchronize = oSharedPreferences.getBoolean("todaysynchronize", false);
        istomorrowSynchronize = oSharedPreferences.getBoolean("tomorrowsynchronize", false);
        ismadeSynchronize = oSharedPreferences.getBoolean("madesynchronize", false);

    }

    public static String getCurrentDate(Context _Context) {
        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(calendar.getTime());

        return formattedDate;
    }

    public static String Removenull(String URL1) {
        String URL = URL1.replace(" ", "%20").trim();
        System.out.println("**********" + URL);
        return URL;
    }

    public static String maidSynchronizeURL() {
        String URL = BASEURL + "getmaids?imei=" + strPhoneIMEI;
        // URL =
        // "http://www.emaid.info/mm/mm/api/getmaids?imei=356938035643809";

        return Removenull(URL);
    }

    public static String getLogin(String code) {

        String URL = BASEURL + "settings?imei=" + strPhoneIMEI + "&accesscode=" + code;
        // URL =
        // "http://www.emaid.info/mm/mm/api/settings?imei=356938035643809&accesscode=MMCC3412";

        return Removenull(URL);

    }

    public static String getCustomerdetails(String date) {

        String URL = BASEURL + "getschedule?imei=" + strPhoneIMEI + "&day=" + date;

        // URL =
        // "http://www.emaid.info/mm/mm/api/getschedule?imei=356938035643809&day="
        // +
        // date;
        return Removenull(URL);

    }

    public static String getMAidAttendance(String status, String maid_id) {

        String URL = BASEURL + "maidattendance?imei=" + strPhoneIMEI + "&maid_id=" + maid_id + "&status=" + status;
        // URL =
        // "http://www.emaid.info/mm/mm/api/updatemaidstatus/?imei=356938035643809&maid_id="
        // + maid_id + "&status=" + status;
        return Removenull(URL);
    }

    public static String getCustomerAttendance(String status, String booking_id) {

        String URL = BASEURL + "updateservicestatus?imei=" + strPhoneIMEI + "&booking_id=" + booking_id + "&status=" + status;
        // URL =
        // "http://www.emaid.info/mm/mm/api/updatebookingstatus/?imei=356938035643809&booking_id="
        // + booking_id + "&status=" + status;
        return Removenull(URL);
    }

    public static String getCustomerAttendance(String status, String booking_id, String payment, String amount, String outstanding_amount, String receipt_no, String method)

    {

        String URL = BASEURL + "updateservicestatus?imei=" + strPhoneIMEI + "&booking_id=" + booking_id + "&status=" + status + "&payment=" + payment + "&amount=" + amount
                + "&outstanding_amount=" + outstanding_amount + "&receipt_no=" + receipt_no + "&method=" + method;
        // URL =
        // "http://www.emaid.info/mm/mm/api/updatebookingstatus/?imei=356938035643809&booking_id="
        // + booking_id + "&status=" + status;
        return Removenull(URL);
    }

    public static String getTransferMaid(String zone_id, String maid_id) {

        String URL = BASEURL + "transfermaid?imei=" + strPhoneIMEI + "&maid_id=" + maid_id + "&zone_id=" + zone_id + "&day=1";
        // URL =
        // "http://www.emaid.info/mm/mm/api/transfermaid?imei=356938035643809&maid_id="
        // + maid_id + "&zone_id=" + zone_id;
        return Removenull(URL);
    }

    public static String getChangemaidRequest(String booking_id) {

        String URL = BASEURL + "maidchangerequest?imei=" + strPhoneIMEI + "&booking_id=" + booking_id;

        return Removenull(URL);
    }

    public static String getLocationUpdateURl(String imei, String latitude, String longitude, String speed) {
        String URL = BASEURL + "tabletlocations?imei=" + imei + "&latitude=" + latitude + "&longitude=" + longitude + "&speed=" + speed;

        return Removenull(URL);
    }

    public static String getPaymentHistory() {
        String URL = BASEURL + "getpayments?imei=" + strPhoneIMEI;

        return Removenull(URL);
    }

    public static String getAttendanceReport() {
        String URL = BASEURL + "getmaidattendacereport?imei=" + strPhoneIMEI;

        return Removenull(URL);
    }

}
