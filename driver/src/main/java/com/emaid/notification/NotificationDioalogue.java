package com.emaid.notification;

import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.emaid.R;
import com.emaid.database.customer.SynchronizeCustomer;

public class NotificationDioalogue {
	
	static Dialog dialog1 ;

	public static void Notification(final Context _context, String _strHeading, String _strMessage)
	{

	

		 dialog1 = new Dialog(_context);

		dialog1.setContentView(R.layout.customdialogue_error);

		dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		dialog1.setCancelable(true);
		TextView textViewHeading = (TextView) dialog1.findViewById(R.id.customeerror_heading);
		TextView textViewContent = (TextView) dialog1.findViewById(R.id.customeerror_message);
		Button buttonOk = (Button) dialog1.findViewById(R.id.customeerror_btn);
		textViewHeading.setText(_strHeading.trim());
		textViewContent.setText(_strMessage.trim());

		buttonOk.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				NotificationManager nMgr = (NotificationManager) _context.getSystemService(Context.NOTIFICATION_SERVICE);
				nMgr.cancelAll();
				SynchronizeCustomer.diologueSynchronize(_context, "1");
				dialog1.dismiss();
				
			}
		});

		dialog1.show();

	}


}
