package com.emaid;

import static com.google.gson.internal.$Gson$Types.arrayOf;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.emaid.R;
import com.emaid.azinova.EmaidLogin;
import com.emaid.baseinterface.BaseInterface;
import com.emaid.baseinterface.GlobalManager;
import com.emaid.database.customer.SynchronizeCustomer;
import com.emaid.database.maid.ImageLoderutil;
import com.emaid.utils.CommonUtil;
import com.emaid.utils.URLUtil;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.util.ArrayList;
import java.util.List;

public class Login extends BaseActivity implements BaseInterface
{
	Button _btnSubmit;
	EditText _etxtCode;
	ImageView _ivEMEI;

	private final static int REQUEST_CHECK_SETTINGS_GPS=0x1;
	private final static int REQUEST_ID_MULTIPLE_PERMISSIONS=0x2;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		GlobalManager.getdata().setCurrentActivity("Login");

		//onLocation();

		initUI();
		clickFunction();
	}

/*
	private void onLocation() {
		if (ActivityCompat.checkSelfPermission(
				this,
				android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
				ActivityCompat.checkSelfPermission(
						this,
						android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions( this, new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION  },
					101);


		}



	}
*/

	/*@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);


	}*/

	@Override
	public void initUI()
	{
		_btnSubmit = (Button) findViewById(R.id.login_btn_submit001);
		_etxtCode = (EditText) findViewById(R.id.login_etxt_code);
		_ivEMEI = (ImageView) findViewById(R.id.login_ivchar);
		

	}

	@Override
	public void clickFunction()
	{

		_btnSubmit.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				getData(URLUtil.getLogin(_etxtCode.getText().toString().trim()));

			}
		});

		_ivEMEI.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				CommonUtil.CustomeDialogue(Login.this, "IMEI NUMBER", URLUtil.strPhoneIMEI);
			}
		});

	}

	public void getData(String URL)
	{
		AsyncHttpClient client = new AsyncHttpClient();
		client.setTimeout(30000);
		client.get(URL, new AsyncHttpResponseHandler()
		{
			@Override
			public void onStart()
			{
				super.onStart();
				CommonUtil.progressDialogue(Login.this);
				CommonUtil.dialog.show();
			}

			@Override
			public void onFailure(Throwable arg0, String arg1)
			{
				super.onFailure(arg0, arg1);
				System.out.println("*******"+arg0);
				URLUtil.edit.putBoolean("login", false);
				URLUtil.edit.commit();
				CommonUtil.CustomeDialogue(Login.this, "Error", "Network Error");

			}

			@Override
			public void onSuccess(int arg0, String arg1)
			{
				// TODO Auto-generated method stub
				super.onSuccess(arg0, arg1);
				try
				{

					Gson gson = new Gson();
					EmaidLogin oEmaidLogin = gson.fromJson(arg1, EmaidLogin.class);

					if (oEmaidLogin.getStatus().equals("success"))
					{

						for (int i = 0; i < oEmaidLogin.getZones().size(); i++)
						{
							if (oEmaidLogin.getZones().get(i).getZone_id().equalsIgnoreCase(oEmaidLogin.getMy_zone()))
							{

								URLUtil.edit.putString("zonename", oEmaidLogin.getZones().get(i).getZone_name());
							}

						}

//						URLUtil.edit.putString("tabletid", oEmaidLogin.getMy_tablet());
						
						URLUtil.edit.putString("zone", arg1);
						URLUtil.edit.putBoolean("login", true);
						URLUtil.edit.commit();
						URLUtil.activateSharepreference(Login.this);

						ImageLoderutil.retriveMaid(Login.this);
						if (!SynchronizeCustomer.retriveCustomer(Login.this) && (!ImageLoderutil.isSynchronize()))
						{
							Intent oIntent = new Intent(Login.this, Synchronize.class);
							oIntent.putExtra("page", "0");
							startActivity(oIntent);

							finish();
							overridePendingTransition(R.anim.slide_out_front, R.anim.slide_in_front);

						}
						else if (URLUtil.strSynchronizeDate.equalsIgnoreCase(""))
						{
							Intent oIntent = new Intent(Login.this, Synchronize.class);
							oIntent.putExtra("page", "0");
							startActivity(oIntent);
							finish();
							overridePendingTransition(R.anim.slide_out_front, R.anim.slide_in_front);
						}
						else
						{
							Intent oIntent = new Intent(Login.this, HomepageActivity.class);

							startActivity(oIntent);

							finish();
							overridePendingTransition(R.anim.slide_out_front, R.anim.slide_in_front);
						}

					}
					else
					{
						URLUtil.edit.putBoolean("login", false);
						URLUtil.edit.commit();
						CommonUtil.CustomeDialogue(Login.this, "Error", oEmaidLogin.getStatus());
					}

				}
				catch (Exception e)
				{
					URLUtil.edit.putBoolean("login", false);
					URLUtil.edit.commit();
					CommonUtil.CustomeDialogue(Login.this, "Error", "Network Error");
				}

			}

			@Override
			public void onFinish()
			{
				super.onFinish();
				CommonUtil.dialog.dismiss();

			}

		});
	}

}
