package com.emaid;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;

import com.emaid.R;
import com.emaid.baseinterface.GlobalManager;
import com.emaid.database.customer.SynchronizeCustomer;
import com.emaid.database.maid.ImageLoderutil;
import com.emaid.database.maid.MySQLiteHelper;
import com.emaid.location.LocationAlarm;
import com.emaid.utils.URLUtil;

public class SplashScreen extends BaseActivity {

	private static int SPLASH_TIME_OUT = 3000;
	MySQLiteHelper mySQLiteHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splashscreen);
		mySQLiteHelper = new MySQLiteHelper(SplashScreen.this);
		mySQLiteHelper.getWritableDatabase();
		GlobalManager.getdata().setCurrentActivity("SplashScreen");
		URLUtil.activateSharepreference(SplashScreen.this);
		LocationAlarm.initAlarm(SplashScreen.this);


		/*String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

		if(!provider.contains("gps")){ //if gps is disabled
			final Intent poke = new Intent();
			poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
			poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
			poke.setData(Uri.parse("3"));
			sendBroadcast(poke);
		}*/




		new Handler().postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				
				
					if (URLUtil.oSharedPreferences.getBoolean("login", false))
					{

						ImageLoderutil.retriveMaid(SplashScreen.this);
						if (!SynchronizeCustomer.retriveCustomer(SplashScreen.this) && (!ImageLoderutil.isSynchronize()))
						{
							Intent oIntent = new Intent(SplashScreen.this, Synchronize.class);
							oIntent.putExtra("page", "0");
							startActivity(oIntent);
							finish();
							overridePendingTransition(R.anim.slide_out_front, R.anim.slide_in_front);

						}
						else if (!URLUtil.strSynchronizeDate.equalsIgnoreCase(URLUtil.getCurrentDate(SplashScreen.this)))
						{
							Intent oIntent = new Intent(SplashScreen.this, Synchronize.class);
							oIntent.putExtra("page", "0");
							startActivity(oIntent);
							finish();
							overridePendingTransition(R.anim.slide_out_front, R.anim.slide_in_front);
						}
						else
						{
							Intent oIntent = new Intent(SplashScreen.this, HomepageActivity.class);

							startActivity(oIntent);

							finish();
							overridePendingTransition(R.anim.slide_out_front, R.anim.slide_in_front);
						}

					}
					else
					{
						Intent oIntent = new Intent(SplashScreen.this, Login.class);
						startActivity(oIntent);
						finish();
						overridePendingTransition(R.anim.slide_out_front, R.anim.slide_in_front);
					}
				}
				

			
		}, SPLASH_TIME_OUT);

	}




}
